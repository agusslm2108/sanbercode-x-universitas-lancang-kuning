@extends('layout.master')
@section('judul')
Detail cast
@endsection
@section('content')
<a class="nav-link" href="/Tcast" data-widget="iframe-scrollright"><i class="fas fa-angle-double-left">back</i></a>
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Detail Cast</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 10px">id</th>
            <th>nama</th>
            <th>umur</th>
            <th>bio</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{{ $cast->id }}</td>
            <td>{{ $cast->nama }}</td>
            <td>{{ $cast->umur }}</td>
            <td>{{ $cast->bio }}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div> 
@endsection