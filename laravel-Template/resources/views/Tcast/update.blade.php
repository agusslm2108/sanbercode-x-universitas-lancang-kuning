@extends('layout.master')
@section('judul')
Cast
@endsection
@section('content')

<a class="nav-link" href="/Tcast" data-widget="iframe-scrollright"><i class="fas fa-angle-double-left">back</i></a>

<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">tambah data cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/Tcast/{{ $cast->id }}" method="POST">
        @csrf
        @method('put')
      <div class="card-body">
        <div class="form-group">
          <label>nama</label>
          <input type="text" name="nama" class="form-control" value="{{ $cast->nama }}">
        </div>
        @error('nama')
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-ban"></i> Alert!</h5>
            {{ $message }}
          </div>
        @enderror
        <div class="form-group">
            <label>umur</label>
            <input name="umur" class="form-control" value="{{ $cast->umur }}">
          </div>
          @error('umur')
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-ban"></i> Alert!</h5>
            {{ $message }}
          </div>
          @enderror
          <div class="form-group">
            <label>bio</label>
            <textarea name="bio" class="form-control" rows="5">{{ $cast->bio }}</textarea>
          </div>
          @error('bio')
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-ban"></i> Alert!</h5>
            {{ $message }}
          </div>
          @enderror
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
@endsection