@extends('layout.master')
@section('judul')
Table Cast
@endsection
@push('scripts')
<script src="{{asset ('/Template/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{asset ('/Template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
  <script>
    $(function () {
        $("#example1").DataTable();
    });
  </script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
@section('content')

<div class="card">
  <div class="card-header d-flex justify-content-between align-items-center">
    <h3 class="card-title">Tabel data Cast</h3>
    
  </div>
  <div class="card-body">
    @if(session('success'))
            <div class="alert alert-success">{{session('success')}}</div>
    @endif
    <div class="new" style="margin-bottom:5px">
      <a href="/Tcast/create" class="btn btn-primary">TambahData</a>
  </div>
    <table class="table table-bordered" id="example1">
      <thead style="text-align: center">
        <tr>
          <th style="width: 10px">no</th>
          <th>nama</th>
          <th>umur</th>
          <th>bio</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($cast as $key => $item)
        <tr>
          <td>{{ $key+1}}</td>
          <td>{{ $item->nama }}</td>
          <td>{{ $item->umur }}</td>
          <td>{{ Str::limit($item->bio, 30) }}</td>
          <td class="d-flex justify-content-center align-items-center">
            <a href="/Tcast/{{ $item->id }}" class="btn btn-primary btn-sm" style="margin-right: 20px" >
            show</a>
            <a href="/Tcast/{{ $item->id }}/edit" class="btn btn-info btn-sm">
            edit</a>
            <form action="{{ route('Tcast.destroy', $item->id) }}" method="POST" style="margin-left: 20px">
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-danger btn-sm"  onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">
               delete</button>
            </form>
          </td>
        </tr>
        @empty
        <tr>
          <td colspan="5" class="text-center">Data kosong</td>
        </tr>
        @endforelse
      </tbody>
    </table>
  </div>
</div>

@endsection