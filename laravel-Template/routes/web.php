<?php

use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('layout.master');
});

route::get('/dashboard', [HomeController::class, 'dashbord']);
route::get('/table', [HomeController::class, 'table']);
route::get('/data-tables', [HomeController::class, 'dataTable']);

//Route untuk tabel cast CRUD
Route::resource('Tcast', CastController::class);
