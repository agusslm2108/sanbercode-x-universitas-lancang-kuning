<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('form');
    }

    public function proses(Request $request)
    // Mengambil nilai dari request
    {
        $firtsname = $request->input('firtsname');
        $lastname = $request->input('lastname');

        return view('home', [
            'firtsname' => $firtsname,
            'lastname' => $lastname,
        ]);
    }
}
